package au.redwoodit.kubess2i;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(scanBasePackages = "au.redwoodit.kubess2i")
@EnableDiscoveryClient
public class KubesS2iApplication {

    public static void main(String[] args) {
        SpringApplication.run(KubesS2iApplication.class, args);
    }

}
